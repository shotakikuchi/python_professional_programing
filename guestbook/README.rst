============================
ゲストブックアプリ
============================

目的
====

WebブラウザでコメントするWebアプリケーションの練習。

ツールのバージョン
================

Python 3.5.1 :: Anaconda 4.0.0 (x86_64)
=======================================

リポジトリからコードを取得し、その下にconda環境を用意します::

$ git clone https://shotakikuchi@bitbucket.org/shotakikuchi/python_professional_programing.git
$ cd python_professional_programing/guestbook
$ conda create -n guestbook python=3.5.1
$ conda info --envs
$ source activate guestbook
$ (( guestbook )) pip install .
$ (( guestbook )) guestbook
    * Running on http://127.0.0.1:8888/

開発手順
=======


開発用インストール
===============

1.チェックアウトする
2.以下の手順でインストールする

$ pip install -e .


依存ライブラリ変更時
==================
1. ``setup.py`` の ``install_requires`` を更新する
2. 以下の手順で環境を更新する::
    conda uninstall -n guestbook --all
    conda create -n guestbook python=3.5.1
    source activate guestbook
    (( guestbook )) pip install -e .
    (( guestbook )) pip install -r requirements.txt

3. setup.pyとrequirements.txtをリポジトリにコミットする。

パッケージ作成
================

構成
====

guestbook
+-- LICENSE.txt
+-- MANIFEST.in
+-- README.rst
+-- guestbook

|   +-- __init__.py

|   +-- static/main.css

|   +-- templates/index.html

+-- setup.py

手順
====

# http://www.task-notes.com/entry/20151116/1447642800
### Create Packege

$ conda create -n guestbook python=3.5.1
$ source activate guestbook
$ conda info --envs
$ python setup.py sdist
$ pip install -e .

### python setup.py sdist : ソース配布パッケージを作る
$ python setup.py sdist
