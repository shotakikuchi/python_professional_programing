# coding: utf-8
import shelve
from datetime import datetime  # この行を追加
from flask import Flask, request, render_template, redirect, escape, Markup

DATA_FILE = 'guestbook.dat'
application = Flask(__name__)


def load_data():
  """投稿されたデータを返します
  """
  # shelveモジュールでデータベースファイルを開きます。
  database = shelve.open(DATA_FILE)

  # greeting_listを返します。データがなければ空のリストを返します。
  greeting_list = database.get('greeting_list', [])
  database.close()
  return greeting_list


def save_data(name, comment, create_at):
  """投稿データを保存します
  """
  # shelve モジュールでデータベースファイルを開きます。
  database = shelve.open(DATA_FILE)

  greeting_list = load_data()

  greeting_list.insert(0, {
    'name': name,
    'comment': comment,
    'create_at': create_at,
  })

  # データベースを更新します。
  database['greeting_list'] = greeting_list
  database.close()


@application.route('/')
def index():
  """トップページ
   テンプレートを使用してページを表示します。
  """
  # 投稿データを読み込みます。
  greeting_list = load_data()
  return render_template('index.html', greeting_list=greeting_list)


@application.route('/post', methods=['POST'])
def post():
  """投稿用URL
  """
  # 投稿されたデータを取得
  name = request.form.get('name')  # 名前
  comment = request.form.get('comment')  # コメント
  create_at = datetime.now()  # 投稿日時（現在時間）

  # データを保存します。
  save_data(name, comment, create_at)

  # 保存後はトップページにリダイレクトします。
  return redirect('/')


@application.template_filter('nl2br')
def nl2br_filter(s):
  """改行文字をbrタグに置き換えるテンプレートフィルタ
  """
  return escape(s).replace('\n', Markup('<br>'))


@application.template_filter('datetime_fmt')
def datetime_fmt_filter(dt):
  """datetimeオブジェクトを見やすい表示にするテンプレートフィルタ
  """
  return dt.strftime('%Y/%m/%d %H:%M:%S')

def main():
  application.run('127.0.0.1', 8888)

if __name__ == '__main__':
  # IPアドレスの127.0.0.1の8000番ポートでアプリケーションを実行します。
  application.run('127.0.0.1', 8888, debug=True)
